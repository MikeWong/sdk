package ch.mwong.domain.framework.usecase;

import rx.Subscriber;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class DefaultSubscriber<T> extends Subscriber<T> {

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {

	}

	@Override
	public void onNext(T t) {

	}
}
