package ch.mwong.domain.framework.executor;

import rx.Scheduler;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface PostExecutionThread {

	Scheduler getScheduler();

}
