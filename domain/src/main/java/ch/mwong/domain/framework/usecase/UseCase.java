package ch.mwong.domain.framework.usecase;

import ch.mwong.domain.framework.executor.PostExecutionThread;
import ch.mwong.domain.framework.executor.ThreadExecutor;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public abstract class UseCase {

	private final ThreadExecutor threadExecutor;
	private final PostExecutionThread postExecutionThread;

	private Subscription subscription = Subscriptions.empty();

	protected UseCase(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
		this.threadExecutor = threadExecutor;
		this.postExecutionThread = postExecutionThread;
	}

	protected abstract Observable buildUseCaseObservable();

	@SuppressWarnings("unchecked")
	public void execute(Subscriber useCaseSubscriber) {
		this.subscription = buildUseCaseObservable()
				.subscribeOn(Schedulers.from(threadExecutor))
				.observeOn(postExecutionThread.getScheduler())
				.subscribe(useCaseSubscriber);
	}

	public void unsubscribe() {
		if (!subscription.isUnsubscribed()) {
			subscription.unsubscribe();
		}
	}

}
