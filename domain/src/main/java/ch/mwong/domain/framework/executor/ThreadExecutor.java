package ch.mwong.domain.framework.executor;

import java.util.concurrent.Executor;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface ThreadExecutor extends Executor {
}
