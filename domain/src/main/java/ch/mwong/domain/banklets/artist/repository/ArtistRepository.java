package ch.mwong.domain.banklets.artist.repository;

import java.util.List;

import ch.mwong.domain.banklets.artist.data.Artist;
import rx.Observable;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface ArtistRepository {

	Observable<List<Artist>> findArtists(String query);

}
