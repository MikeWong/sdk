package ch.mwong.domain.banklets.artist.usecase;

import javax.inject.Inject;

import ch.mwong.domain.banklets.artist.repository.ArtistRepository;
import ch.mwong.domain.framework.executor.PostExecutionThread;
import ch.mwong.domain.framework.executor.ThreadExecutor;
import ch.mwong.domain.framework.usecase.UseCase;
import rx.Observable;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class FindArtistsUseCase extends UseCase {

	private String query;
	private final ArtistRepository repository;

	@Inject
	public FindArtistsUseCase(String query, ArtistRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
		super(threadExecutor, postExecutionThread);
		this.query = query;
		this.repository = repository;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	@Override
	protected Observable buildUseCaseObservable() {
		return repository.findArtists(query);
	}


}
