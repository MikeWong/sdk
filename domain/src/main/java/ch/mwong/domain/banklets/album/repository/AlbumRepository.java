package ch.mwong.domain.banklets.album.repository;

import java.util.List;

import ch.mwong.domain.banklets.album.data.Album;
import rx.Observable;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface AlbumRepository {

	Observable<List<Album>> findAlbums(String query);

}
