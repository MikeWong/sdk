package ch.mwong.domain.banklets.album.usecase;

import javax.inject.Inject;

import ch.mwong.domain.banklets.album.repository.AlbumRepository;
import ch.mwong.domain.framework.executor.PostExecutionThread;
import ch.mwong.domain.framework.executor.ThreadExecutor;
import ch.mwong.domain.framework.usecase.UseCase;
import rx.Observable;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class FindAlbumsUseCase extends UseCase {

	private String query;
	private final AlbumRepository repository;

	@Inject
	public FindAlbumsUseCase(String query, AlbumRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
		super(threadExecutor, postExecutionThread);
		this.query = query;
		this.repository = repository;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	@Override
	protected Observable buildUseCaseObservable() {
		return repository.findAlbums(query);
	}


}
