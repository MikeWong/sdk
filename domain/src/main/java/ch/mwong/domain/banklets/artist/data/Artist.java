package ch.mwong.domain.banklets.artist.data;

import java.util.ArrayList;
import java.util.List;

import ch.mwong.domain.banklets.shared.data.Image;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class Artist {

	private String id;
	private String name;
	private List<Image> images = new ArrayList<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public void addImage(Image image) {
		images.add(image);
	}
}
