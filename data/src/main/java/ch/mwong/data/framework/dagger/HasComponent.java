package ch.mwong.data.framework.dagger;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface HasComponent<C> {

	C getComponent();

}
