package ch.mwong.data.framework.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
