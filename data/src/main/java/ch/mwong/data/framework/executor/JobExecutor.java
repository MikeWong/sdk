package ch.mwong.data.framework.executor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import ch.mwong.domain.framework.executor.ThreadExecutor;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@Singleton
public class JobExecutor implements ThreadExecutor {

	private static final int INITIAL_POOL_SIZE = 3;
	private static final int MAX_POOL_SIZE = 5;

	private static final int KEEP_ALIVE_TIME = 10;
	private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

	private final BlockingQueue<Runnable> workQueue;

	private final ThreadFactory threadFactory;
	private final ThreadPoolExecutor threadPoolExecutor;

	@Inject
	public JobExecutor() {
		workQueue = new LinkedBlockingQueue<>();
		threadFactory = new JobThreadFactory();
		threadPoolExecutor = new ThreadPoolExecutor(INITIAL_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, workQueue, threadFactory);
	}

	@Override
	public void execute(Runnable runnable) {
		if (runnable == null) {
			throw new IllegalArgumentException("Runnable must not be null");
		}

		threadPoolExecutor.execute(runnable);
	}

	private static class JobThreadFactory implements ThreadFactory {
		private static final String THREAD_NAME = "android_";
		private int counter = 0;

		@Override
		public Thread newThread(Runnable runnable) {
			return new Thread(runnable, THREAD_NAME + counter);
		}
	}

}
