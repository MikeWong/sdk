package ch.mwong.data.banklets.album.repository.datasource;

import android.content.Context;

import javax.inject.Inject;

import ch.mwong.data.banklets.album.rest.AlbumRestClient;
import ch.mwong.data.framework.dagger.PerActivity;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@PerActivity
public class AlbumDataStoreFactory {

	private final Context context;

	@Inject
	public AlbumDataStoreFactory(Context context) {
		if (context == null) {
			throw new IllegalArgumentException("Context must not be null");
		}
		this.context = context;
	}

	public AlbumDataStore create() {
		AlbumDataStore albumDataStore = createRestDataStore();
		return albumDataStore;
	}

	private AlbumDataStore createRestDataStore() {
		AlbumRestClient client = new AlbumRestClient();
		return new RestAlbumDataStore(client.getService());
	}

}
