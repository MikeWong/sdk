
package ch.mwong.data.banklets.album.entity;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class AlbumSearch {

    @Expose
    private Albums albums;

    /**
     * 
     * @return
     *     The albums
     */
    public Albums getAlbums() {
        return albums;
    }

    /**
     * 
     * @param albums
     *     The albums
     */
    public void setAlbums(Albums albums) {
        this.albums = albums;
    }

}
