
package ch.mwong.data.banklets.album.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

import ch.mwong.data.banklets.shared.entity.ImageEntity;

@Generated("org.jsonschema2pojo")
public class AlbumEntity {

    @SerializedName("album_type")
    @Expose
    private String albumType;
    @SerializedName("available_markets")
    @Expose
    private List<String> availableMarkets = new ArrayList<String>();
    @SerializedName("external_urls")
    @Expose
    private ExternalUrls externalUrls;
    @Expose
    private String href;
    @Expose
    private String id;
    @SerializedName("images")
    @Expose
    private List<ImageEntity> imageEntities = new ArrayList<ImageEntity>();
    @Expose
    private String name;
    @Expose
    private String type;
    @Expose
    private String uri;

    /**
     * 
     * @return
     *     The albumType
     */
    public String getAlbumType() {
        return albumType;
    }

    /**
     * 
     * @param albumType
     *     The album_type
     */
    public void setAlbumType(String albumType) {
        this.albumType = albumType;
    }

    /**
     * 
     * @return
     *     The availableMarkets
     */
    public List<String> getAvailableMarkets() {
        return availableMarkets;
    }

    /**
     * 
     * @param availableMarkets
     *     The available_markets
     */
    public void setAvailableMarkets(List<String> availableMarkets) {
        this.availableMarkets = availableMarkets;
    }

    /**
     * 
     * @return
     *     The externalUrls
     */
    public ExternalUrls getExternalUrls() {
        return externalUrls;
    }

    /**
     * 
     * @param externalUrls
     *     The external_urls
     */
    public void setExternalUrls(ExternalUrls externalUrls) {
        this.externalUrls = externalUrls;
    }

    /**
     * 
     * @return
     *     The href
     */
    public String getHref() {
        return href;
    }

    /**
     * 
     * @param href
     *     The href
     */
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The images
     */
    public List<ImageEntity> getImageEntities() {
        return imageEntities;
    }

    /**
     * 
     * @param imageEntities
     *     The images
     */
    public void setImageEntities(List<ImageEntity> imageEntities) {
        this.imageEntities = imageEntities;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * 
     * @param uri
     *     The uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

}
