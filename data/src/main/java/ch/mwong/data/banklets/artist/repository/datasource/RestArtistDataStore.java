package ch.mwong.data.banklets.artist.repository.datasource;

import java.util.List;

import ch.mwong.data.banklets.artist.entity.ArtistEntity;
import ch.mwong.data.banklets.artist.entity.ArtistSearch;
import ch.mwong.data.banklets.artist.rest.ArtistRestService;
import rx.Observable;
import rx.Subscriber;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class RestArtistDataStore implements ArtistDataStore {

	private ArtistRestService service;

	public RestArtistDataStore(ArtistRestService service) {
		this.service = service;
	}

	@Override
	public Observable<List<ArtistEntity>> findArtists(final String query) {
		final ArtistSearch search = service.findArtists(query);
		return Observable.create(new Observable.OnSubscribe<List<ArtistEntity>>() {
			@Override
			public void call(Subscriber<? super List<ArtistEntity>> subscriber) {
				List<ArtistEntity> artistEntities = search.getArtists().getArtistEntities();
				subscriber.onNext(artistEntities);
			}
		});
	}
}
