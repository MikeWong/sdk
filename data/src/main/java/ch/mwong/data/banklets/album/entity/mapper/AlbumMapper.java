package ch.mwong.data.banklets.album.entity.mapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ch.mwong.data.banklets.album.entity.AlbumEntity;
import ch.mwong.data.banklets.shared.entity.ImageEntity;
import ch.mwong.data.framework.dagger.PerActivity;
import ch.mwong.domain.banklets.album.data.Album;
import ch.mwong.domain.banklets.shared.data.Image;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@PerActivity
public class AlbumMapper {

	@Inject public AlbumMapper() {}

	public List<Album> transform(List<AlbumEntity> entities) {
		List<Album> albumList = new ArrayList<>(entities.size());
		Album album;

		for (AlbumEntity entity : entities) {
			album = transform(entity);
			if (album != null) {
				albumList.add(album);
			}
		}

		return albumList;
	}

	private Album transform(AlbumEntity entity) {
		Album album = null;

		if (entity != null) {
			album = new Album();
			album.setId(entity.getId());
			album.setName(entity.getName());
			for (ImageEntity imageEntity : entity.getImageEntities()) {
				Image image = new Image();
				image.setUrl(imageEntity.getUrl());
				image.setWidth(imageEntity.getWidth());
				image.setHeight(imageEntity.getHeight());
				album.addImage(image);
			}
		}

		return album;
	}

}
