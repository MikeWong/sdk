package ch.mwong.data.banklets.album.rest;

import retrofit.RestAdapter;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class AlbumRestClient {

	private static final String BASE_URL = "https://api.spotify.com/v1";

	private AlbumRestService service;

	public AlbumRestClient() {
		RestAdapter adapter = new RestAdapter.Builder()
				.setEndpoint(BASE_URL)
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.build();

		service = adapter.create(AlbumRestService.class);
	}

	public AlbumRestService getService() {
		return service;
	}

}
