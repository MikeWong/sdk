package ch.mwong.data.banklets.artist.repository.datasource;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import ch.mwong.data.banklets.artist.rest.ArtistRestClient;
import ch.mwong.data.framework.dagger.PerActivity;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@PerActivity
public class ArtistDataStoreFactory {

	private final Context context;

	@Inject
	public ArtistDataStoreFactory(Context context) {
		if (context == null) {
			throw new IllegalArgumentException("Context must not be null");
		}
		this.context = context;
	}

	public ArtistDataStore create() {
		ArtistDataStore artistDataStore = createRestDataStore();
		return artistDataStore;
	}

	private ArtistDataStore createRestDataStore() {
		ArtistRestClient client = new ArtistRestClient();
		return new RestArtistDataStore(client.getService());
	}

}
