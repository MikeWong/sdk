package ch.mwong.data.banklets.artist.rest;

import ch.mwong.data.banklets.artist.entity.ArtistSearch;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface ArtistRestService {

	@GET("/search?type=artist")
	ArtistSearch findArtists(@Query("q") String name);

}
