package ch.mwong.data.banklets.artist.rest;

import retrofit.RestAdapter;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class ArtistRestClient {

	private static final String BASE_URL = "https://api.spotify.com/v1";

	private ArtistRestService service;

	public ArtistRestClient() {
		RestAdapter adapter = new RestAdapter.Builder()
				.setEndpoint(BASE_URL)
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.build();

		service = adapter.create(ArtistRestService.class);
	}

	public ArtistRestService getService() {
		return service;
	}

}
