package ch.mwong.data.banklets.album.repository.datasource;

import java.util.List;

import ch.mwong.data.banklets.album.entity.AlbumEntity;
import ch.mwong.data.banklets.album.entity.AlbumSearch;
import ch.mwong.data.banklets.album.rest.AlbumRestService;
import rx.Observable;
import rx.Subscriber;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class RestAlbumDataStore implements AlbumDataStore {

	private AlbumRestService service;

	public RestAlbumDataStore(AlbumRestService service) {
		this.service = service;
	}

	@Override
	public Observable<List<AlbumEntity>> findAlbums(String query) {
		final AlbumSearch search = service.findAlbums(query);
		return rx.Observable.create(new Observable.OnSubscribe<List<AlbumEntity>>() {
			@Override
			public void call(Subscriber<? super List<AlbumEntity>> subscriber) {
				List<AlbumEntity> albumEntities = search.getAlbums().getAlbumEntities();
				subscriber.onNext(albumEntities);
			}
		});
	}

}
