package ch.mwong.data.banklets.artist.entity.mapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ch.mwong.data.banklets.artist.entity.ArtistEntity;
import ch.mwong.data.banklets.shared.entity.ImageEntity;
import ch.mwong.data.framework.dagger.PerActivity;
import ch.mwong.domain.banklets.artist.data.Artist;
import ch.mwong.domain.banklets.shared.data.Image;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@PerActivity
public class ArtistMapper {

	@Inject public ArtistMapper() {}

	public List<Artist> transform(List<ArtistEntity> entities) {
		List<Artist> artistList = new ArrayList<>(entities.size());
		Artist artist;

		for (ArtistEntity entity : entities) {
			artist = transform(entity);
			if (artist != null) {
				artistList.add(artist);
			}
		}

		return artistList;
	}

	private Artist transform(ArtistEntity entity) {
		Artist artist = null;

		if (entity != null) {
			artist = new Artist();
			artist.setId(entity.getId());
			artist.setName(entity.getName());
			for (ImageEntity i : entity.getImageEntities()) {
				Image image = new Image();
				image.setUrl(i.getUrl());
				image.setWidth(i.getWidth());
				image.setHeight(i.getHeight());
				artist.addImage(image);
			}
		}

		return artist;
	}

}
