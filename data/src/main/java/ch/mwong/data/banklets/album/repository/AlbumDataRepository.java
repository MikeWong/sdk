package ch.mwong.data.banklets.album.repository;

import java.util.List;

import javax.inject.Inject;

import ch.mwong.data.banklets.album.entity.AlbumEntity;
import ch.mwong.data.banklets.album.entity.mapper.AlbumMapper;
import ch.mwong.data.banklets.album.repository.datasource.AlbumDataStore;
import ch.mwong.data.banklets.album.repository.datasource.AlbumDataStoreFactory;
import ch.mwong.data.framework.dagger.PerActivity;
import ch.mwong.domain.banklets.album.data.Album;
import ch.mwong.domain.banklets.album.repository.AlbumRepository;
import rx.Observable;
import rx.functions.Func1;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@PerActivity
public class AlbumDataRepository implements AlbumRepository {

	private final AlbumDataStoreFactory factory;
	private final AlbumMapper mapper;
	private final Func1<List<AlbumEntity>, List<Album>> albumEntityMapper = new Func1<List<AlbumEntity>, List<Album>>() {
		@Override
		public List<Album> call(List<AlbumEntity> entities) {
			return mapper.transform(entities);
		}
	};

	@Inject
	public AlbumDataRepository(AlbumDataStoreFactory factory, AlbumMapper mapper) {
		this.factory = factory;
		this.mapper = mapper;
	}

	@Override
	public Observable<List<Album>> findAlbums(String query) {
		final AlbumDataStore albumDataStore = factory.create();
		return albumDataStore.findAlbums(query).map(albumEntityMapper);
	}
}
