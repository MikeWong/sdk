package ch.mwong.data.banklets.artist.repository.datasource;

import java.util.List;

import ch.mwong.data.banklets.artist.entity.ArtistEntity;
import rx.Observable;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface ArtistDataStore {

	Observable<List<ArtistEntity>> findArtists(String query);
}
