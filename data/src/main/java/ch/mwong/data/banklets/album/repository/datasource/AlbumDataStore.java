package ch.mwong.data.banklets.album.repository.datasource;

import java.util.List;

import ch.mwong.data.banklets.album.entity.AlbumEntity;
import rx.Observable;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface AlbumDataStore {

	Observable<List<AlbumEntity>> findAlbums(String query);

}
