package ch.mwong.data.banklets.album.rest;

import ch.mwong.data.banklets.album.entity.AlbumSearch;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface AlbumRestService {

	@GET("/search?type=album")
	AlbumSearch findAlbums(@Query("q") String name);

}
