package ch.mwong.data.banklets.artist.repository;

import java.util.List;

import javax.inject.Inject;

import ch.mwong.data.banklets.artist.entity.ArtistEntity;
import ch.mwong.data.banklets.artist.entity.mapper.ArtistMapper;
import ch.mwong.data.banklets.artist.repository.datasource.ArtistDataStore;
import ch.mwong.data.banklets.artist.repository.datasource.ArtistDataStoreFactory;
import ch.mwong.data.framework.dagger.PerActivity;
import ch.mwong.domain.banklets.artist.data.Artist;
import ch.mwong.domain.banklets.artist.repository.ArtistRepository;
import rx.Observable;
import rx.functions.Func1;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@PerActivity
public class ArtistDataRepository implements ArtistRepository {

	private final ArtistDataStoreFactory factory;
	private final ArtistMapper mapper;
	private final Func1<List<ArtistEntity>, List<Artist>> artistEntityMapper = new Func1<List<ArtistEntity>, List<Artist>>() {
		@Override
		public List<Artist> call(List<ArtistEntity> entities) {
			return mapper.transform(entities);
		}
	};

	@Inject
	public ArtistDataRepository(ArtistDataStoreFactory factory, ArtistMapper mapper) {
		this.factory = factory;
		this.mapper = mapper;
	}

	@Override
	public Observable<List<Artist>> findArtists(String query) {
		final ArtistDataStore artistDataStore = factory.create();
		return artistDataStore.findArtists(query).map(artistEntityMapper);
	}
}
