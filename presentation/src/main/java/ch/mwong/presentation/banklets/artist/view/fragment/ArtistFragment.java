package ch.mwong.presentation.banklets.artist.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import javax.inject.Inject;

import ch.mwong.presentation.R;
import ch.mwong.presentation.banklets.artist.dagger.components.ArtistComponent;
import ch.mwong.presentation.banklets.artist.dagger.components.DaggerArtistComponent;
import ch.mwong.presentation.banklets.artist.model.ArtistModel;
import ch.mwong.presentation.banklets.artist.presenter.ArtistPresenter;
import ch.mwong.presentation.banklets.artist.view.ArtistView;
import ch.mwong.presentation.banklets.artist.view.adapter.ArtistAdapter;
import ch.mwong.presentation.framework.view.activity.BaseActivity;
import ch.mwong.presentation.framework.view.fragment.BaseFragment;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class ArtistFragment extends BaseFragment implements ArtistView, View.OnClickListener {

	@Inject ArtistPresenter presenter;

	private EditText queryField;
	private Button submitButton;
	private RecyclerView artistList;

	private ArtistAdapter artistAdapter;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_artist_list, container, false);

		queryField = (EditText) root.findViewById(R.id.artist_list_query);
		submitButton = (Button) root.findViewById(R.id.artist_list_submit);
		artistList = (RecyclerView) root.findViewById(R.id.artist_list_recyclerview);

		initLayout();

		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initialize();
	}

	@Override
	public void onResume() {
		super.onResume();
		presenter.resume();
	}

	@Override
	public void onPause() {
		super.onPause();
		presenter.pause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		presenter.destroy();
	}

	private void initialize() {
		ArtistComponent component = DaggerArtistComponent.builder()
				.applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
				.activityModule(((BaseActivity) getActivity()).getActivityModule())
				.build();
		component.inject(this);
		presenter.setView(this);
	}

	private void initLayout() {
		submitButton.setOnClickListener(this);
		artistList.setLayoutManager(new LinearLayoutManager(getActivity()));
	}

	private void loadArtists(final String query) {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				presenter.initialize(query);
				return null;
			}
		}.execute();
	}

	@Override
	public void renderArtistList(List<ArtistModel> artists) {
		if (artists != null) {
			if (artistAdapter == null) {
				artistAdapter = new ArtistAdapter(getActivity(), artists);
			} else {
				artistAdapter.setArtists(artists);
			}

			artistList.setAdapter(artistAdapter);
		}
	}

	@Override
	public void showLoading(boolean show) {
		if (show) {
			getActivity().setProgressBarIndeterminateVisibility(true);
		} else {
			getActivity().setProgressBarIndeterminateVisibility(false);
		}
	}

	@Override
	public Context getContext() {
		return getActivity();
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == submitButton.getId()) {
			String query = queryField.getText().toString();
			loadArtists(query);
		}
	}
}
