package ch.mwong.presentation.banklets.album.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ch.mwong.presentation.R;
import ch.mwong.presentation.banklets.album.model.AlbumModel;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder> {

	private final Context context;
	private List<AlbumModel> albums;
	private final LayoutInflater inflater;

	public AlbumAdapter(Context context, List<AlbumModel> albums) {
		this.context = context;
		this.albums = albums;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View root = inflater.inflate(R.layout.row_album, parent, false);
		return new AlbumViewHolder(root);
	}

	@Override
	public void onBindViewHolder(AlbumViewHolder albumViewHolder, int position) {
		final AlbumModel model = albums.get(position);

		if (model.getImageModels() != null && model.getImageModels().size() > 0) {
			Picasso.with(context).load(model.getImageModels().get(0).getUrl()).into(albumViewHolder.coverView);
		} else {
			albumViewHolder.coverView.setImageResource(R.mipmap.ic_launcher);
		}
		albumViewHolder.nameView.setText(model.getName());
	}

	@Override
	public int getItemCount() {
		return albums.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void setAlbums(List<AlbumModel> albums) {
		this.albums = albums;
		notifyDataSetChanged();
	}

	static class AlbumViewHolder extends RecyclerView.ViewHolder {
		private ImageView coverView;
		private TextView nameView;

		public AlbumViewHolder(View root) {
			super(root);
			coverView = (ImageView) root.findViewById(R.id.album_cover);
			nameView = (TextView) root.findViewById(R.id.album_name);
		}
	}
}
