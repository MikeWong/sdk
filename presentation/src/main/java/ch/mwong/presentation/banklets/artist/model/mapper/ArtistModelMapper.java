package ch.mwong.presentation.banklets.artist.model.mapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import ch.mwong.domain.banklets.artist.data.Artist;
import ch.mwong.domain.banklets.shared.data.Image;
import ch.mwong.presentation.banklets.artist.model.ArtistModel;
import ch.mwong.presentation.banklets.shared.model.ImageModel;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class ArtistModelMapper {

	@Inject public ArtistModelMapper() {}

	public List<ArtistModel> transform(List<Artist> artists) {
		List<ArtistModel> models;

		if (artists != null && !artists.isEmpty()) {
			models = new ArrayList<>(artists.size());
			for (Artist artist : artists) {
				models.add(transform(artist));
			}
		} else {
			models = Collections.emptyList();
		}

		return models;
	}

	private ArtistModel transform(Artist artist) {
		if (artist == null) {
			throw new IllegalArgumentException("Artist must not be null");
		}

		ArtistModel model = new ArtistModel();
		model.setId(artist.getId());
		model.setName(artist.getName());
		for (Image i : artist.getImages()) {
			ImageModel image = new ImageModel();
			image.setUrl(i.getUrl());
			image.setWidth(i.getWidth());
			image.setHeight(i.getHeight());
			model.addImage(image);
		}

		return model;
	}

}
