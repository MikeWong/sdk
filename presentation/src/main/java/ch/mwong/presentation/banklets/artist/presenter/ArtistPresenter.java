package ch.mwong.presentation.banklets.artist.presenter;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import ch.mwong.data.framework.dagger.PerActivity;
import ch.mwong.domain.banklets.artist.data.Artist;
import ch.mwong.domain.banklets.artist.usecase.FindArtistsUseCase;
import ch.mwong.domain.framework.usecase.DefaultSubscriber;
import ch.mwong.domain.framework.usecase.UseCase;
import ch.mwong.presentation.banklets.artist.model.ArtistModel;
import ch.mwong.presentation.banklets.artist.model.mapper.ArtistModelMapper;
import ch.mwong.presentation.banklets.artist.view.ArtistView;
import ch.mwong.presentation.framework.presenter.Presenter;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 29.05.2015
 */
@PerActivity
public class ArtistPresenter extends DefaultSubscriber<List<Artist>> implements Presenter {

	private ArtistView artistView;

	private String query;

	private FindArtistsUseCase useCase;
	private ArtistModelMapper mapper;

	@Inject
	public ArtistPresenter(@Named("findArtists") UseCase findArtistsUseCase, ArtistModelMapper mapper) {
		this.useCase = (FindArtistsUseCase) findArtistsUseCase;
		this.mapper = mapper;
	}

	public void setView(@NonNull ArtistView artistView) {
		this.artistView = artistView;
	}

	@Override
	public void resume() {}

	@Override
	public void pause() {}

	@Override
	public void destroy() {
		useCase.unsubscribe();
	}

	public void initialize(String query) {
		this.query = query;
		findArtists();
	}

	private void showArtistsInView(List<Artist> artists) {
		final List<ArtistModel> artistModels = mapper.transform(artists);
		artistView.renderArtistList(artistModels);
	}

	private void findArtists() {
		useCase.setQuery(query);
		useCase.execute(this);
	}

	@Override
	public void onCompleted() {
		super.onCompleted();
	}

	@Override
	public void onError(Throwable e) {
		super.onError(e);
	}

	@Override
	public void onNext(List<Artist> artists) {
		showArtistsInView(artists);
	}

}
