package ch.mwong.presentation.banklets.album.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import javax.inject.Inject;

import ch.mwong.presentation.R;
import ch.mwong.presentation.banklets.album.dagger.components.AlbumComponent;
import ch.mwong.presentation.banklets.album.dagger.components.DaggerAlbumComponent;
import ch.mwong.presentation.banklets.album.model.AlbumModel;
import ch.mwong.presentation.banklets.album.presenter.AlbumPresenter;
import ch.mwong.presentation.banklets.album.view.AlbumView;
import ch.mwong.presentation.banklets.album.view.adapter.AlbumAdapter;
import ch.mwong.presentation.framework.view.activity.BaseActivity;
import ch.mwong.presentation.framework.view.fragment.BaseFragment;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class AlbumFragment extends BaseFragment implements AlbumView, View.OnClickListener {

	@Inject AlbumPresenter presenter;

	private EditText queryField;
	private Button submitButton;
	private RecyclerView albumList;

	private AlbumAdapter albumAdapter;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_album_list, container, false);

		queryField = (EditText) root.findViewById(R.id.album_list_query);
		submitButton = (Button) root.findViewById(R.id.album_list_submit);
		albumList = (RecyclerView) root.findViewById(R.id.album_list_recyclerview);

		initLayout();

		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initialize();
	}

	@Override
	public void onResume() {
		super.onResume();
		presenter.resume();
	}

	@Override
	public void onPause() {
		super.onPause();
		presenter.pause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		presenter.destroy();
	}

	private void initialize() {
		AlbumComponent component = DaggerAlbumComponent.builder()
				.applicationComponent(((BaseActivity) getActivity()).getApplicationComponent())
				.activityModule(((BaseActivity) getActivity()).getActivityModule())
				.build();
		component.inject(this);
		presenter.setView(this);
	}

	private void initLayout() {
		submitButton.setOnClickListener(this);
		albumList.setLayoutManager(new LinearLayoutManager(getActivity()));
	}

	private void loadAlbums(final String query) {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				presenter.initialize(query);
				return null;
			}
		}.execute();
	}

	@Override
	public void renderAlbumList(List<AlbumModel> albums) {
		if (albums != null) {
			if (albumAdapter == null) {
				albumAdapter = new AlbumAdapter(getActivity(), albums);
			} else {
				albumAdapter.setAlbums(albums);
			}

			albumList.setAdapter(albumAdapter);
		}
	}

	@Override
	public void showLoading(boolean show) {
		if (show) {
			getActivity().setProgressBarIndeterminateVisibility(true);
		} else {
			getActivity().setProgressBarIndeterminateVisibility(false);
		}
	}

	@Override
	public Context getContext() {
		return getActivity();
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == submitButton.getId()) {
			String query = queryField.getText().toString();
			loadAlbums(query);
		}
	}
}
