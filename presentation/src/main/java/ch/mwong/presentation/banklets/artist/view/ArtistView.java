package ch.mwong.presentation.banklets.artist.view;

import java.util.List;

import ch.mwong.presentation.banklets.artist.model.ArtistModel;
import ch.mwong.presentation.framework.view.LoadDataView;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface ArtistView extends LoadDataView {

	void renderArtistList(List<ArtistModel> artists);

}
