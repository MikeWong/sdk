package ch.mwong.presentation.banklets.artist.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ch.mwong.presentation.R;
import ch.mwong.presentation.banklets.artist.model.ArtistModel;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ArtistViewHolder> {

	private final Context context;
	private List<ArtistModel> artists;
	private final LayoutInflater inflater;

	public ArtistAdapter(Context context, List<ArtistModel> artists) {
		this.context = context;
		this.artists = artists;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public ArtistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View root = inflater.inflate(R.layout.row_artist, parent, false);
		return new ArtistViewHolder(root);
	}

	@Override
	public void onBindViewHolder(ArtistViewHolder artistViewHolder, int position) {
		final ArtistModel model = artists.get(position);

		if (model.getImageModels() != null && model.getImageModels().size() > 0) {
			Picasso.with(context).load(model.getImageModels().get(0).getUrl()).into(artistViewHolder.coverView);
		} else {
			artistViewHolder.coverView.setImageResource(R.mipmap.ic_launcher);
		}
		artistViewHolder.nameView.setText(model.getName());
	}

	@Override
	public int getItemCount() {
		return artists.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void setArtists(List<ArtistModel> artists) {
		this.artists = artists;
		notifyDataSetChanged();
	}

	static class ArtistViewHolder extends RecyclerView.ViewHolder {
		private ImageView coverView;
		private TextView nameView;

		public ArtistViewHolder(View root) {
			super(root);
			coverView = (ImageView) root.findViewById(R.id.artist_cover);
			nameView = (TextView) root.findViewById(R.id.artist_name);
		}
	}
}
