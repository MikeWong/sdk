package ch.mwong.presentation.banklets.album.model;

import java.util.ArrayList;
import java.util.List;

import ch.mwong.presentation.banklets.shared.model.ImageModel;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class AlbumModel {

	private String id;
	private String name;
	private List<ImageModel> imageModels = new ArrayList<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ImageModel> getImageModels() {
		return imageModels;
	}

	public void setImageModels(List<ImageModel> imageModels) {
		this.imageModels = imageModels;
	}

	public void addImage(ImageModel imageModel) {
		imageModels.add(imageModel);
	}
}
