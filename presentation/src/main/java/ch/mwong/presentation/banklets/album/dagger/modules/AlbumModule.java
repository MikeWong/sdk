package ch.mwong.presentation.banklets.album.dagger.modules;

import javax.inject.Named;

import ch.mwong.data.banklets.album.repository.AlbumDataRepository;
import ch.mwong.data.framework.dagger.PerActivity;
import ch.mwong.domain.banklets.album.repository.AlbumRepository;
import ch.mwong.domain.banklets.album.usecase.FindAlbumsUseCase;
import ch.mwong.domain.framework.executor.PostExecutionThread;
import ch.mwong.domain.framework.executor.ThreadExecutor;
import ch.mwong.domain.framework.usecase.UseCase;
import dagger.Module;
import dagger.Provides;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@Module
public class AlbumModule {

	private String query;

	public AlbumModule() {}

	public AlbumModule(String query) {
		this.query = query;
	}

	@Provides
	@PerActivity
	@Named("findAlbums")
	UseCase provideFindAlbumsUseCase(AlbumRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
		return new FindAlbumsUseCase(query, repository, threadExecutor, postExecutionThread);
	}

	@Provides
	@PerActivity
	AlbumRepository provideAlbumRepository(AlbumDataRepository albumDataRepository) {
		return albumDataRepository;
	}

}
