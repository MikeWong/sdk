package ch.mwong.presentation.banklets.album.dagger.components;

import ch.mwong.domain.banklets.album.repository.AlbumRepository;
import ch.mwong.presentation.banklets.album.dagger.modules.AlbumModule;
import ch.mwong.presentation.banklets.album.view.fragment.AlbumFragment;
import ch.mwong.data.framework.dagger.PerActivity;
import ch.mwong.presentation.framework.dagger.components.ApplicationComponent;
import ch.mwong.presentation.framework.dagger.modules.ActivityModule;
import dagger.Component;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, AlbumModule.class})
public interface AlbumComponent {

	void inject(AlbumFragment fragment);

	AlbumRepository albumRepository();

}
