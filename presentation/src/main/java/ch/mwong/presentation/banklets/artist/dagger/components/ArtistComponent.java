package ch.mwong.presentation.banklets.artist.dagger.components;

import ch.mwong.data.framework.dagger.PerActivity;
import ch.mwong.domain.banklets.artist.repository.ArtistRepository;
import ch.mwong.presentation.banklets.artist.dagger.modules.ArtistModule;
import ch.mwong.presentation.banklets.artist.view.fragment.ArtistFragment;
import ch.mwong.presentation.framework.dagger.components.ApplicationComponent;
import ch.mwong.presentation.framework.dagger.modules.ActivityModule;
import dagger.Component;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, ArtistModule.class})
public interface ArtistComponent {

	void inject(ArtistFragment fragment);

	ArtistRepository artistRepository();

}
