package ch.mwong.presentation.banklets.album.view;

import java.util.List;

import ch.mwong.presentation.banklets.album.model.AlbumModel;
import ch.mwong.presentation.framework.view.LoadDataView;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface AlbumView extends LoadDataView {

	void renderAlbumList(List<AlbumModel> albums);

}
