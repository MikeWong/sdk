package ch.mwong.presentation.banklets.album.model.mapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import ch.mwong.domain.banklets.album.data.Album;
import ch.mwong.domain.banklets.shared.data.Image;
import ch.mwong.presentation.banklets.album.model.AlbumModel;
import ch.mwong.presentation.banklets.shared.model.ImageModel;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class AlbumModelMapper {

	@Inject public AlbumModelMapper() {}

	public List<AlbumModel> transform(List<Album> albums) {
		List<AlbumModel> models;

		if (albums != null && !albums.isEmpty()) {
			models = new ArrayList<>(albums.size());
			for (Album album : albums) {
				models.add(transform(album));
			}
		} else {
			models = Collections.emptyList();
		}

		return models;
	}

	private AlbumModel transform(Album album) {
		if (album == null) {
			throw new IllegalArgumentException("Album must not be null");
		}

		AlbumModel model = new AlbumModel();
		model.setId(album.getId());
		model.setName(album.getName());
		for (Image i : album.getImages()) {
			ImageModel image = new ImageModel();
			image.setUrl(i.getUrl());
			image.setWidth(i.getWidth());
			image.setHeight(i.getHeight());
			model.addImage(image);
		}

		return model;
	}

}
