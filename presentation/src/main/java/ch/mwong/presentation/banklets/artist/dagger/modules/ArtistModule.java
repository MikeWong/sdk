package ch.mwong.presentation.banklets.artist.dagger.modules;

import javax.inject.Named;

import ch.mwong.data.banklets.artist.repository.ArtistDataRepository;
import ch.mwong.data.framework.dagger.PerActivity;
import ch.mwong.domain.banklets.artist.repository.ArtistRepository;
import ch.mwong.domain.banklets.artist.usecase.FindArtistsUseCase;
import ch.mwong.domain.framework.executor.PostExecutionThread;
import ch.mwong.domain.framework.executor.ThreadExecutor;
import ch.mwong.domain.framework.usecase.UseCase;
import dagger.Module;
import dagger.Provides;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@Module
public class ArtistModule {

	private String query;

	public ArtistModule() {}

	public ArtistModule(String query) {
		this.query = query;
	}

	@Provides
	@PerActivity
	@Named("findArtists")
	UseCase provideFindArtistsUseCase(ArtistRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
		return new FindArtistsUseCase(query, repository, threadExecutor, postExecutionThread);
	}

	@Provides
	@PerActivity
	ArtistRepository provideAlbumRepository(ArtistDataRepository artistDataRepository) {
		return artistDataRepository;
	}

}
