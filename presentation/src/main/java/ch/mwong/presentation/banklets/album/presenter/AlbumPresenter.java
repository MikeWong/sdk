package ch.mwong.presentation.banklets.album.presenter;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import ch.mwong.domain.banklets.album.data.Album;
import ch.mwong.domain.banklets.album.usecase.FindAlbumsUseCase;
import ch.mwong.domain.framework.usecase.DefaultSubscriber;
import ch.mwong.domain.framework.usecase.UseCase;
import ch.mwong.presentation.banklets.album.model.AlbumModel;
import ch.mwong.presentation.banklets.album.model.mapper.AlbumModelMapper;
import ch.mwong.data.framework.dagger.PerActivity;
import ch.mwong.presentation.banklets.album.view.AlbumView;
import ch.mwong.presentation.framework.presenter.Presenter;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@PerActivity
public class AlbumPresenter extends DefaultSubscriber<List<Album>> implements Presenter {

	private AlbumView albumView;

	private String query;

	private FindAlbumsUseCase useCase;
	private AlbumModelMapper mapper;

	@Inject
	public AlbumPresenter(@Named("findAlbums") UseCase findAlbumsUseCase, AlbumModelMapper mapper) {
		this.useCase = (FindAlbumsUseCase) findAlbumsUseCase;
		this.mapper = mapper;
	}

	public void setView(@NonNull AlbumView albumView) {
		this.albumView = albumView;
	}

	@Override
	public void resume() {}

	@Override
	public void pause() {}

	@Override
	public void destroy() {
		useCase.unsubscribe();
	}

	public void initialize(String query) {
		this.query = query;
		findAlbums();
	}

	private void showAlbumsInView(List<Album> albums) {
		final List<AlbumModel> albumModels = mapper.transform(albums);
		albumView.renderAlbumList(albumModels);
	}

	private void findAlbums() {
		useCase.setQuery(query);
		useCase.execute(this);
	}

	@Override
	public void onCompleted() {
		super.onCompleted();
	}

	@Override
	public void onError(Throwable e) {
		super.onError(e);
	}

	@Override
	public void onNext(List<Album> albums) {
		showAlbumsInView(albums);
	}
}
