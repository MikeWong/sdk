package ch.mwong.presentation.framework.navigation;

import android.app.Fragment;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;

import javax.inject.Inject;
import javax.inject.Singleton;

import ch.mwong.presentation.R;
import ch.mwong.presentation.banklets.album.view.fragment.AlbumFragment;
import ch.mwong.presentation.banklets.artist.view.fragment.ArtistFragment;
import ch.mwong.presentation.framework.view.activity.BaseActivity;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@Singleton
public class Navigator {

	public static final String AUTHORITY = "AVQ";
	private static final int ARTIST = 1;
	private static final int ALBUM = 2;

	private UriMatcher uriMatcher;

	@Inject
	public Navigator() {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(AUTHORITY, "artist", ARTIST);
		uriMatcher.addURI(AUTHORITY, "album", ALBUM);
	}

	public void navigateTo(Context ctx, Uri uri) {
		if (ctx != null) {
			int res = uriMatcher.match(uri);
			Fragment fragment;
			switch (res) {
				case ARTIST:
					fragment = new ArtistFragment();
					break;
				case ALBUM:
					fragment = new AlbumFragment();
					break;
				default:
					throw new IllegalArgumentException("Navigator received unknown uri: " + uri);
			}
			((BaseActivity) ctx).addFragment(R.id.main_fragment_container, fragment);
		}
	}

}
