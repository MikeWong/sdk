package ch.mwong.presentation.framework.view.fragment;

import android.app.Fragment;
import android.os.Bundle;

import ch.mwong.data.framework.dagger.HasComponent;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class BaseFragment extends Fragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@SuppressWarnings("unchecked")
	protected <C> C getComponent(Class<C> componentType) {
		return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
	}
}
