package ch.mwong.presentation.framework.view;

import android.content.Context;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface LoadDataView {

	void showLoading(boolean show);

	Context getContext();

}
