package ch.mwong.presentation.framework.dagger.components;

import android.content.Context;

import javax.inject.Singleton;

import ch.mwong.domain.framework.executor.PostExecutionThread;
import ch.mwong.domain.framework.executor.ThreadExecutor;
import ch.mwong.presentation.framework.dagger.modules.ApplicationModule;
import ch.mwong.presentation.framework.view.activity.BaseActivity;
import dagger.Component;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

	void inject(BaseActivity baseActivity);

	Context context();
	ThreadExecutor threadExecutor();
	PostExecutionThread postExecutionThread();

}
