package ch.mwong.presentation.framework.presenter;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public interface Presenter {

	void resume();

	void pause();

	void destroy();

}
