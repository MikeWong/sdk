package ch.mwong.presentation.framework.dagger.modules;

import android.app.Activity;

import ch.mwong.data.framework.dagger.PerActivity;
import dagger.Module;
import dagger.Provides;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@Module
public class ActivityModule {

	private final Activity activity;

	public ActivityModule(Activity activity) {
		this.activity = activity;
	}

	@Provides
	@PerActivity
	Activity activity() {
		return activity;
	}

}
