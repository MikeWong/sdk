package ch.mwong.presentation.framework;

import javax.inject.Inject;
import javax.inject.Singleton;

import ch.mwong.domain.framework.executor.PostExecutionThread;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@Singleton
public class UIThread implements PostExecutionThread {

	@Inject public UIThread() {}

	@Override
	public Scheduler getScheduler() {
		return AndroidSchedulers.mainThread();
	}

}
