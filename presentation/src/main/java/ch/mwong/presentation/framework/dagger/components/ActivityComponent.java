package ch.mwong.presentation.framework.dagger.components;

import android.app.Activity;

import ch.mwong.data.framework.dagger.PerActivity;
import ch.mwong.presentation.framework.dagger.modules.ActivityModule;
import dagger.Component;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

	Activity activity();

}
