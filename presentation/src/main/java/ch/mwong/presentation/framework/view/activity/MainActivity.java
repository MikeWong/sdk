package ch.mwong.presentation.framework.view.activity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import ch.mwong.presentation.R;
import ch.mwong.presentation.framework.navigation.Navigator;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 29.05.2015
 */
public class MainActivity extends BaseActivity implements View.OnClickListener {

	private Button artistButton;
	private Button albumButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_main);

		artistButton = (Button) findViewById(R.id.main_button_artist);
		albumButton = (Button) findViewById(R.id.main_button_album);

		artistButton.setOnClickListener(this);
		albumButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();

		if (id == R.id.main_button_artist) {
			Uri uri = new Uri.Builder().authority(Navigator.AUTHORITY).path("artist").build();
			navigator.navigateTo(this, uri);
		} else if (id == R.id.main_button_album) {
			Uri uri = new Uri.Builder().authority(Navigator.AUTHORITY).path("album").build();
			navigator.navigateTo(this, uri);
		}
	}

}
