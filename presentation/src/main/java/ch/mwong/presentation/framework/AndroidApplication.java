package ch.mwong.presentation.framework;

import android.app.Application;

import ch.mwong.presentation.framework.dagger.components.ApplicationComponent;
import ch.mwong.presentation.framework.dagger.components.DaggerApplicationComponent;
import ch.mwong.presentation.framework.dagger.modules.ApplicationModule;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class AndroidApplication extends Application {

	private ApplicationComponent applicationComponent;

	@Override
	public void onCreate() {
		super.onCreate();
		initializeInjector();
	}

	private void initializeInjector() {
		applicationComponent = DaggerApplicationComponent.builder()
				.applicationModule(new ApplicationModule(this))
				.build();
	}

	public ApplicationComponent getApplicationComponent() {
		return applicationComponent;
	}
}
