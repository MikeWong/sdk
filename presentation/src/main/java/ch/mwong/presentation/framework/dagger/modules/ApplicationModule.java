package ch.mwong.presentation.framework.dagger.modules;

import android.content.Context;

import javax.inject.Singleton;

import ch.mwong.data.framework.executor.JobExecutor;
import ch.mwong.domain.framework.executor.PostExecutionThread;
import ch.mwong.domain.framework.executor.ThreadExecutor;
import ch.mwong.presentation.framework.AndroidApplication;
import ch.mwong.presentation.framework.UIThread;
import dagger.Module;
import dagger.Provides;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
@Module
public class ApplicationModule {

	private final AndroidApplication application;

	public ApplicationModule(AndroidApplication application) {
		this.application = application;
	}

	@Provides
	@Singleton
	Context provideApplicationContext() {
		return application;
	}

	@Provides
	@Singleton
	ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
		return jobExecutor;
	}

	@Provides
	@Singleton
	PostExecutionThread providePostExecutionThread(UIThread uiThread) {
		return uiThread;
	}

}
