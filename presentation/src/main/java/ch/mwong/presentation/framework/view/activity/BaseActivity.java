package ch.mwong.presentation.framework.view.activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import javax.inject.Inject;

import ch.mwong.presentation.framework.AndroidApplication;
import ch.mwong.presentation.framework.dagger.components.ApplicationComponent;
import ch.mwong.presentation.framework.dagger.modules.ActivityModule;
import ch.mwong.presentation.framework.navigation.Navigator;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 28.05.2015
 */
public class BaseActivity extends Activity {

	@Inject Navigator navigator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getApplicationComponent().inject(this);
	}

	public void addFragment(int containerId, Fragment fragment) {
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(containerId, fragment);
//		transaction.add(containerId, fragment);
		transaction.commit();
	}

	public ApplicationComponent getApplicationComponent() {
		return ((AndroidApplication) getApplication()).getApplicationComponent();
	}

	public ActivityModule getActivityModule() {
		return new ActivityModule(this);
	}

}
