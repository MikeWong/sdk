package ch.mwong.demoapp.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ch.mwong.demoapp.R;
import ch.mwong.demoapp.ui.fragment.ArtistListFragment;

/**
 * http://www.lastfm.de/api/intro
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 17.04.2015
 */
public class ArtistListActivity extends ActionBarActivity {

	@InjectView(R.id.toolbar) Toolbar toolbar;
	@InjectView(R.id.fragment_container) FrameLayout fragmentContainer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_artist_list);
		ButterKnife.inject(this);
		setSupportActionBar(toolbar);

		getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ArtistListFragment()).commit();
	}

}
