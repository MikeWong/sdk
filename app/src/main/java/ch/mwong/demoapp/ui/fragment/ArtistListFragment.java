package ch.mwong.demoapp.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;
import ch.mwong.demoapp.R;
import ch.mwong.demoapp.adapter.ArtistAdapter;
import ch.mwong.demoapp.data.artist.Search;
import ch.mwong.demoapp.rest.BusCallback;
import ch.mwong.demoapp.rest.SpotifyClient;
import retrofit.RetrofitError;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 17.04.2015
 */
public class ArtistListFragment extends Fragment {

	@InjectView(R.id.artist_list) ListView artistList;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_artist_fragment, container, false);
		ButterKnife.inject(this, view);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		BusCallback<Search> callback = new BusCallback<Search>() {
			@Override
			public void onSuccess(Search query) {
				ArtistAdapter adapter = new ArtistAdapter(getActivity(), query.getResults().getArtistmatches().getArtist());
				artistList.setAdapter(adapter);
			}

			@Override
			public void onFailure(RetrofitError error) {
				error.printStackTrace();
			}
		};

		new SpotifyClient().getService().findArtist("akre", callback);
	}

	@OnItemClick(R.id.artist_list)
	public void itemClicked() {
		Toast.makeText(getActivity(), "Not yet implemented", Toast.LENGTH_LONG).show();
	}
}
