
package ch.mwong.demoapp.data.artist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Artists {

    @SerializedName("items")
    @Expose
    private List<Artist> artists = new ArrayList<Artist>();

    /**
     * 
     * @return
     *     The items
     */
    public List<Artist> getArtists() {
        return artists;
    }

    /**
     * 
     * @param artists
     *     The items
     */
    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

}
