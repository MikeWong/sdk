
package ch.mwong.demoapp.data.album;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class AlbumSearch {

    @SerializedName("items")
    @Expose
    private List<Album> albums = new ArrayList<Album>();

    /**
     * 
     * @return
     *     The items
     */
    public List<Album> getAlbums() {
        return albums;
    }

    /**
     * 
     * @param albums
     *     The items
     */
    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

}
