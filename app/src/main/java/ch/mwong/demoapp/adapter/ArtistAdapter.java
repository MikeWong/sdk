package ch.mwong.demoapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ch.mwong.demoapp.R;
import ch.mwong.demoapp.data.artist.Artist;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 17.04.2015
 */
public class ArtistAdapter extends BaseAdapter {

	private Context context;
	private List<Artist> artists;

	public ArtistAdapter(Context context, List<Artist> artists) {
		this.context = context;
		this.artists = artists;
	}

	@Override
	public int getCount() {
		return artists.size();
	}

	@Override
	public Object getItem(int position) {
		return artists.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Artist artist = artists.get(position);
		ViewHolder holder;
		if (convertView != null) {
			// Reuse viewholder
			holder = (ViewHolder) convertView.getTag();
		} else {
			// Create new viewholder
			convertView = LayoutInflater.from(context).inflate(R.layout.row_artist, null);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		}

		// Show remote artist image
		Glide.with(context).load(artist.getImage("medium").getText()).placeholder(R.mipmap.ic_launcher).into(holder.image);
		holder.name.setText(artist.getName());

		return convertView;
	}

	protected static class ViewHolder {
		@InjectView(R.id.artist_image) ImageView image;
		@InjectView(R.id.artist_name) TextView name;

		public ViewHolder(View view) {
			ButterKnife.inject(this, view);
		}
	}
}
