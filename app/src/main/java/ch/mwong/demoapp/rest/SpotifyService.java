package ch.mwong.demoapp.rest;

import ch.mwong.demoapp.data.album.AlbumSearch;
import ch.mwong.demoapp.data.artist.ArtistSearch;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 17.04.2015
 */
public interface SpotifyService {

	@GET("search?type=artist")
	void searchArtist(@Query("q") String name, Callback<ArtistSearch> callback);

	@GET("artists/{id}/albums?album_type=album")
	void getAlbums(@Path("id") String id, Callback<AlbumSearch> callback);

}
