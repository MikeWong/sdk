package ch.mwong.demoapp.rest;

import retrofit.RestAdapter;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 17.04.2015
 */
public class SpotifyClient {

	private static final String BASE_URL = "https://api.spotify.com/v1/";

	private SpotifyService service;

	public SpotifyClient() {
		RestAdapter adapter = new RestAdapter.Builder()
				.setEndpoint(BASE_URL)
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.build();

		service = adapter.create(SpotifyService.class);
	}

	public SpotifyService getService() {
		return service;
	}

}
