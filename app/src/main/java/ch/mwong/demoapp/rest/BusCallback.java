package ch.mwong.demoapp.rest;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 22.04.2015
 */
public abstract class BusCallback<T> implements Callback<T> {

	private EventBus mBus;

	public BusCallback() {
		this(EventBus.getDefault());
	}

	public BusCallback(EventBus bus) {
		mBus = bus;
		mBus.register(this);
	}

	public abstract void onSuccess(T t);
	public abstract void onFailure(RetrofitError error);

	/**
	 * Success onEvent
	 * @param t
	 */
	public void onEvent(T t) {
		onSuccess(t);
		mBus.unregister(this);
		mBus = null;
	}

	/**
	 * Failure onEvent
	 * @param error
	 */
	public void onEvent(RetrofitError error) {
		onFailure(error);
		mBus.unregister(this);
		mBus = null;
	}

	/**
	 * Success method to do general handling and then post to the callers onSucces
	 * @param t
	 * @param response
	 */
	@Override
	public void success(T t, Response response) {
		mBus.post(t);
	}

	/**
	 * Failure method to do general handling and then post to the callers onFailure
	 * @param error
	 */
	@Override
	public void failure(RetrofitError error) {
		mBus.post(error);
	}
}
