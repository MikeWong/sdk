package ch.mwong.demoapp.application;

import com.orm.SugarApp;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 17.04.2015
 */
public class Application extends SugarApp {

	@Override
	public void onCreate() {
		super.onCreate();
	}
}
