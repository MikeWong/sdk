package ch.mwong.demoapp.application;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 17.04.2015
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

	void inject(Application application);

	// Exposed to subgraphs
	Context context();

}
