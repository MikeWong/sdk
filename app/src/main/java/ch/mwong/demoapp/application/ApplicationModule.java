package ch.mwong.demoapp.application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Mike Wong <m.wong@insign.ch>
 * @version 1.0, 17.04.2015
 */
@Module
public class ApplicationModule {

	private final Application application;

	public ApplicationModule(Application application) {
		this.application = application;
	}

	@Provides
	@Singleton
	public Application provideApplicationContext() {
		return application;
	}
}
